package dam.androidsaulruiz.u4t6contacts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;

import static androidx.core.app.ActivityCompat.startActivityForResult;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private final TextView tvInfo;
    private MyContacts myContacts;
    private Context c;

    static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView tvID;
        TextView tvName;
        TextView tvNumber;
        ImageView imageView;
        View vBarra;
        private OnItemClickListener listener;
        private OnItemLongClickListener longListener;

        public MyViewHolder(View itemview) {
            super(itemview);
            this.tvID = itemview.findViewById(R.id.tvID);
            this.tvName = itemview.findViewById(R.id.tvName);
            this.tvNumber = itemview.findViewById(R.id.tvNumber);
            this.imageView = itemview.findViewById(R.id.ivFoto);
            this.vBarra = itemview.findViewById(R.id.vBarra);

            itemview.setOnClickListener(this);
            itemview.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClickListener(v,getLayoutPosition());
        }
        public void setItemClickListener(OnItemClickListener listener){
            this.listener=listener;
        }
        public void setItemLongClickListener(OnItemLongClickListener listener){
            this.longListener=listener;
        }

        @Override
        public boolean onLongClick(View v) {
            longListener.onItemLongClickListener(v,getLayoutPosition());
            return true;
        }
    }

    MyAdapter(MyContacts myContacts, Context c, TextView tvInfo) {
        this.myContacts = myContacts;
        this.c = c;
        this.tvInfo = tvInfo;
    }

    public interface OnItemClickListener{
        void onItemClickListener(View v, int position);

    }    public interface OnItemLongClickListener{
        void onItemLongClickListener(View v, int position);

    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View tv = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact, null);

        return new MyViewHolder(tv);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.tvID.setText(myContacts.getContactData(position).getId());
        holder.tvName.setText(myContacts.getContactData(position).getName());
        holder.tvNumber.setText(myContacts.getContactData(position).getNumber());
        if (myContacts.getContactData(position).getPhoto() == null) {
            holder.imageView.setImageResource(R.drawable.defaultuser);
        }
        else {
            holder.imageView.setImageURI(Uri.parse(myContacts.getContactData(position).getPhoto()));
        }

        // TODO Ejercicio2 Hacemos que cuando le demos click se modifique el textview con los datos mas si es Home, Work o Mobile
        holder.setItemClickListener((v, position1) -> {

            switch (Integer.parseInt(myContacts.getContactData(position1).getType())){
                case 1:
                    tvInfo.setText(myContacts.getContactData(position1).getName() + " " +
                            myContacts.getContactData(position1).getNumber()+" "+
                            "(HOME)"+"\n_ID: "+myContacts.getContactData(position1).getId()
                            +" CONTACT_ID: "+myContacts.getContactData(position1).getContactID()+
                            " RAW_CONTACT_ID: "+myContacts.getContactData(position1).getRawContactID()
                            +"\nLOOKUP_KEY: "+myContacts.getContactData(position1).getLockupKey());
                    break;
                case 2:
                    tvInfo.setText(myContacts.getContactData(position1).getName() + " "
                            + myContacts.getContactData(position1).getNumber()+" "+
                            "(MOBILE)"+"\n_ID: "+myContacts.getContactData(position1).getId()
                            +" CONTACT_ID: "+myContacts.getContactData(position1).getContactID()+
                            " RAW_CONTACT_ID: "+myContacts.getContactData(position1).getRawContactID()
                            +"\nLOOKUP_KEY: "+myContacts.getContactData(position1).getLockupKey());
                    break;
                case 3:
                    tvInfo.setText(myContacts.getContactData(position1).getName() +
                            " " + myContacts.getContactData(position1).getNumber()+" "+
                            "(WORK)"+"\n_ID: "+myContacts.getContactData(position1).getId()
                            +" CONTACT_ID: "+myContacts.getContactData(position1).getContactID()+
                            " RAW_CONTACT_ID: "+myContacts.getContactData(position1).getRawContactID()
                            +"\nLOOKUP_KEY: "+myContacts.getContactData(position1).getLockupKey());
                    break;
                default:
                    tvInfo.setText(myContacts.getContactData(position1).getName() + " "
                            + myContacts.getContactData(position1).getNumber()+" "+
                            "(OTHER)"+"\n_ID: "+myContacts.getContactData(position1).getId()
                            +" CONTACT_ID: "+myContacts.getContactData(position1).getContactID()+
                            " RAW_CONTACT_ID: "+myContacts.getContactData(position1).getRawContactID()
                            +"\nLOOKUP_KEY: "+myContacts.getContactData(position1).getLockupKey());
                    break;
            }

        });

        //TODO Ejercicio2 Hacemos un long click listener que con el id y el lookupkey le mandamos intent con la ventada de editar
        holder.setItemLongClickListener((v, position1) -> {

            String currentLookupKey = myContacts.getContactData(position1).getLockupKey();

            String currentId = myContacts.getContactData(position1).getId();
            Uri selectedContactUri =
                    ContactsContract.Contacts.getLookupUri(Integer.parseInt(currentId), currentLookupKey);

            // Creates a new Intent to edit a contact
            Intent editIntent = new Intent(Intent.ACTION_EDIT);
            /*
             * Sets the contact URI to edit, and the data type that the
             * Intent must match
             */
            editIntent.setDataAndType(selectedContactUri, ContactsContract.Contacts.CONTENT_ITEM_TYPE);
            editIntent.putExtra("finishActivityOnSaveCompleted", true);
            c.startActivity(editIntent);

        });
    }

    @Override
    public int getItemCount() {
        return myContacts.getCount();
    }


}
