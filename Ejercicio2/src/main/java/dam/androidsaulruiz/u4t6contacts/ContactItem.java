package dam.androidsaulruiz.u4t6contacts;

public class ContactItem {
    private String Name, number, id, photo, contactID, lockupKey, rawContactID, type;

    //TODO Ejercicio2 aumentamos los datos que se le pasan a la clase
    public ContactItem(String name, String number, String id, String photo) {
        Name = name;
        this.number = number;
        this.id = id;
        this.photo = photo;
    }

    public ContactItem(String name, String number, String id, String photo, String contactID, String lockupKey, String rawContactID, String type) {
        Name = name;
        this.number = number;
        this.id = id;
        this.photo = photo;
        this.contactID = contactID;
        this.lockupKey = lockupKey;
        this.rawContactID = rawContactID;
        this.type = type;
    }

    public String getContactID() {
        return contactID;
    }

    public void setContactID(String contactID) {
        this.contactID = contactID;
    }

    public String getLockupKey() {
        return lockupKey;
    }

    public void setLockupKey(String lockupKey) {
        this.lockupKey = lockupKey;
    }

    public String getRawContactID() {
        return rawContactID;
    }

    public void setRawContactID(String rawContactID) {
        this.rawContactID = rawContactID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
