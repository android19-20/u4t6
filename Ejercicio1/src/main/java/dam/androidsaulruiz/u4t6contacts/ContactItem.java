package dam.androidsaulruiz.u4t6contacts;

//TODO Ejercicio1 creamos la clase contactitem
public class ContactItem {
    private String Name, number, id, photo;

    public ContactItem(String name, String number, String id, String photo) {
        Name = name;
        this.number = number;
        this.id = id;
        this.photo = photo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
