package dam.androidsaulruiz.u4t6contacts;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private MyContacts myContacts;
    private Context c;

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvID;
        TextView tvName;
        TextView tvNumber;
        ImageView imageView;
        View vBarra;

        public MyViewHolder(View itemview) {
            super(itemview);
            this.tvID = itemview.findViewById(R.id.tvID);
            this.tvName = itemview.findViewById(R.id.tvName);
            this.tvNumber = itemview.findViewById(R.id.tvNumber);
            this.imageView = itemview.findViewById(R.id.ivFoto);
            this.vBarra = itemview.findViewById(R.id.vBarra);
        }

        public void bind(String id, String name, String number, String foto, Context c) {

            this.tvID.setText(id);
            this.tvName.setText(name);
            this.tvNumber.setText(number);

            //TODO Ejercicio1 cuando no hay foto pone una por defecto
            if (foto == null) {
                this.imageView.setImageResource(R.drawable.defaultuser);
            }
            else {
                this.imageView.setImageURI(Uri.parse(foto));
            }
        }
    }

    MyAdapter(MyContacts myContacts, Context c) {
        this.myContacts = myContacts;
        this.c = c;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View tv = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact, null);

        return new MyViewHolder(tv);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        //holder.tvNumber.setText("xd");
        holder.bind(myContacts.getContactData(position).getId(), myContacts.getContactData(position).getName(), myContacts.getContactData(position).getNumber(), myContacts.getContactData(position).getPhoto(), c);
    }

    @Override
    public int getItemCount() {
        return myContacts.getCount();
    }


}
